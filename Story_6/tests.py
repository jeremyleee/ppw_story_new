
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from django.apps import apps
from .apps import Story6Config

# Create your tests here. class Lab5UnitTest(TestCase):
class Story_6_UnitTest(TestCase):
    def test_Story_6_url_is_exist(self): 			#test url
            response = Client().get('/Story_6/')
            self.assertEqual(response.status_code, 200)
        
    def test_Story_6_using_index_func(self):	#index func
                found = resolve('/Story_6/')
                self.assertEqual(found.func, form)

    def test_submit_url_is_exist(self):
        response = Client().get('/Story_6/aku')
        self.assertEqual(response.status_code,302)

    def test_landing_page_is_completed(self):
        response = Client().get('/Story_6/')
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa Kabar?', html_response)

    def test_apps(self):
        self.assertEqual(Story6Config.name, 'Story_6')
        self.assertEqual(apps.get_app_config('Story_6').name, 'Story_6')

  #  def test_Story_6_post_success_and_render_the_result(self):
   #     test = 'alamak alamak tolong aku'
    #    response = Client().post('',{'status':test})
     #   self.assertEqual(response.status_code, 302)

        #response = Client().get('')
        #html_response = response.content.decode('utf8')
        #self.assertIn(test, html_response)

   # def test_profile_page_is_exist(self):
    #   response = Client().get('/profile')
     #      self.assertEqual(response.status_code, 200)

    #def test_story6_profile_using_index_func(self):
     #   found = resolve('/profile')
      #  self.assertEqual(found.func, profile)
